glasnie_rus = "аеёиоуыэюя"
glasnie_eng = "aeiou"

glasnie = glasnie_rus + glasnie_eng

schethik = [0] * len(glasnie) # делаем с нулями строку счетчика гласных

input_string = input("Введите строку: ").lower()

for i in input_string:
    if i in glasnie:
        index_glasnoi = glasnie.index(i)
        schethik[index_glasnoi] += 1

for i, glasnay in enumerate(glasnie):
    veroyat = schethik[i] / len(input_string)
    print(f"Вероятность появления гласной '{glasnay}': {veroyat:.4f}")

