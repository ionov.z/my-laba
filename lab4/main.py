from sorting import sort

def main():
    array = [1, 2, 3, 4, 5, 6, 7, 8, 9] #элементы массива
    swap = 2 # количество обменов

    print("Исходный массив:", array)

    # Вызываем функцию сортировки
    result = custom_sort(array, swaps)

    # Вывод результата
    print("Результат перестановки:", result)

if __name__ == "__main__":
    main()