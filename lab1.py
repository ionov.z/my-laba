from math import log, sqrt

a = 10**-1
b = 2.1
c = 0.1
d = -3.12
y = (a**b + b**a * log(c) - c * (log(sqrt(a)))) / (2 * b + d)
print(y)